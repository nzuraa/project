<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'auth'], function() {
    route::resource('/developer', 'DeveloperController');
    route::resource('/project', 'ProjectController');
    route::resource('/task', 'TaskController');
});




Route::get('/','PagesController@welcome');

Route::get('/about','PagesController@about');

Route::get('/product','PagesController@product');

Route::get('/contact','PagesController@contact');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
