<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('projectname');
            $table->string('projectdescription');
            $table->string('customer');
            $table->date('startdate');
            $table->date('enddate');
            $table->string('status');
            $table->integer('developer_id')->unsigned()->index()->nullable();
            $table->foreign('developer_id')->references('id')->on('developers');
            $table->timestamps();
        });
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
