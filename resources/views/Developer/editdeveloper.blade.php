@extends('layouts.app')


@section('content') 

<div class="container">
    <a href="{{ url('/developer') }}" class="btn btn-danger"><i class="fas fa-angle-left"></i> Back to Developer</a> 
</div><br>

<div class="container">
    <div class="card  text-white bg-dark mb-3">
    <h5 class="card-header shadow"><i class="fas fa-user-edit"></i> Edit Your Developer</h5>
    <div class="card-body shadow">
        <div class="container">
            <form method="post" action="{{action('DeveloperController@update' , $developer->id)}}" enctype="multipart/form-data">
                <input name="_method" type="hidden" value="PATCH">  @csrf

            
            

            <br>

            <h2 class="text-center"> YOUR DEVELOPER'S DETAIL </h2>

            <br><br>


            <form>
                <div class="form-group row">
                    <label for="name" class="col-4 col-form-label">Name</label> 
                    <div class="col-8">
                      <div class="input-group">
                        <div class="input-group-addon">
                        </div> 
                        <input id="name" name="name" type="text" class="form-control here" value="{{$developer->name}}">
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                      <label for="icnumber" class="col-4 col-form-label">Identification No.</label> 
                      <div class="col-8">
                        <input id="icnumber" name="icnumber" type="text" class="form-control here" value="{{$developer->icnumber}}">
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="streetname" class="col-4 col-form-label">Street Name</label> 
                        <div class="col-8">
                          <input id="streetname" name="streetname"  type="text" class="form-control here" value="{{$developer->streetname}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="postcode" class="col-4 col-form-label">Postcode</label> 
                        <div class="col-8">
                          <input id="postcode" name="postcode" type="text" class="form-control here" value="{{$developer->postcode}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="state" class="col-4 col-form-label">State</label> 
                        <div class="col-8">
                            <select class="form-control here" name=state value={{$developer->state}}>
                              <option value="Johor">Johor</option>
                              <option value="Kedah">Kedah</option>
                              <option value="Kelantan">Kelantan</option>
                              <option value="Kuala Lumpur">Kuala Lumpur</option>
                              <option value="Labuan">Labuan</option>
                              <option value="Melaka">Melaka</option>
                              <option value="Negeri sembilan">Negeri Sembilan</option>
                              <option value="Pahang">Pahang</option>
                              <option value="Penang">Pulau Pinang</option>
                              <option value="Putrajaya">Putrajaya</option>
                              <option value="Perak">Perak</option>
                              <option value="Perlis">Perlis</option>
                              <option value="Sabah">Sabah</option>
                              <option value="Sarawak">Sarawak</option>
                              <option value="Selangor">Selangor</option>
                              <option value="Terengganu">Terengganu</option>
                          </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-4 col-form-label">Email Address</label> 
                        <div class="col-8">
                          <input id="email" name="email" type="text" class="form-control here" value="{{$developer->email}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="phonenumber" class="col-4 col-form-label">Phone No.</label> 
                        <div class="col-8">
                          <input id="phonenumber" name="phonenumber" type="text" class="form-control here" value="{{$developer->phonenumber}}">
                        </div>
                   </div>
                   <div class="form-group row">
                      <label for="workingdate" class="col-4 col-form-label">Start working date</label> 
                      <div class="col-8">
                        <input id="startworking" name="startworking" type="date" class="form-control here" value="{{$developer->startworking}}">
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="skills" class="col-4 col-form-label">Skill</label> 
                        <div class="col-8">
                          <textarea id="skills" name="skills" cols="40" rows="5" class="form-control" >{{$developer->skills}}</textarea>
                        </div>
                      </div> 
                    {{-- <div class="form-group row">
                        <label class="col-4 col-form-label">Skill</label> 
                        <div class="col-8">
                          <div class="form-check">
                            <label class="form-check-label">
                              <input name="skills" type="checkbox" class="form-check-input" value="Python">
                                    Python
                            </label>
                          </div>
                          <div class="form-check">
                            <label class="form-check-label">
                              <input name="skills" type="checkbox" class="form-check-input" value="C">
                                    C
                            </label>
                          </div>
                          <div class="form-check">
                            <label class="form-check-label">
                              <input name="skills" type="checkbox" class="form-check-input" value="C++">
                                    C++
                            </label>
                          </div>
                          <div class="form-check">
                            <label class="form-check-label">
                              <input name="skills" type="checkbox" class="form-check-input" value="Java">
                                    Java
                            </label>
                          </div>
                          <div class="form-check">
                            <label class="form-check-label">
                              <input name="skills" type="checkbox" class="form-check-input" value="Javascript">
                                    Javascript
                            </label>
                          </div>
                          <div class="form-check">
                            <label class="form-check-label">
                              <input name="skills" type="checkbox" class="form-check-input" value="C">
                                    C
                            </label>
                          </div>
                          <div class="form-check">
                            <label class="form-check-label">
                              <input name="skills" type="checkbox" class="form-check-input" value="C++">
                                    C++
                            </label>
                          </div>
                          <div class="form-check">
                            <label class="form-check-label">
                              <input name="skills" type="checkbox" class="form-check-input" value="PHP">
                                    PHP
                            </label>
                          </div>
                          <div class="form-check">
                            <label class="form-check-label">
                              <input name="skills" type="checkbox" class="form-check-input" value="HTML">
                                    HTML
                            </label>
                          </div>
                          <div class="form-check">
                            <label class="form-check-label">
                              <input name="skills" type="checkbox" class="form-check-input" value="CSS">
                                    CSS
                            </label>
                          </div>
                        </div>
                      </div> --}}
                      <div class="form-group row">
                          <label class="col-4">Status</label> 
                          <div class="col-8">
                            <div class="form-check form-check-inline">
                              <label class="form-check-label">
                                <input name="status" type="radio" required="required" class="form-check-input" value="Full Time">
                                      Full Time
                              </label>
                            </div>
                            <div class="form-check form-check-inline">
                              <label class="form-check-label">
                                <input name="status" type="radio" required="required" class="form-check-input" value="Part Time">
                                      Part Time
                              </label>
                            </div>
                          </div>
                        </div> 
                    
                    <div class="form-group row">
                      <div class="offset-4 col-8">
                        <button name="submit" type="submit" class="btn btn-primary">Confirm</button>
                      </div>
                    </div>
                  </form>

@endsection