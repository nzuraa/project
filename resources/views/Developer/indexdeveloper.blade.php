{{-- <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Developers List</title>
<link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>

<body>
    <div class="container">
<br />
@if (\Session::has('success'))
<div class="alert alert-success">
<p>{{ \Session::get('success') }}</p>
</div><br />
@endif


</div>
</body>
</html>
    <div class="container">
        <a href="/developer/create" class="btn btn-info">Create New</a>
        <table class="table">
            <tr>
              <td>Name</td>
              <td>IC Number</td>
              <td>Street Name</td>
              <td>Postcode</td>
              <td>State</td>
              <td>Email</td>
              <td>Phone Number</td>
              <td>Start Working Date</td>
              <td>Skills</td>
              <td>Status</td>
              <td>Action</td>
            </tr>
            @foreach($developer as $developer)
                <tr>
                    <td>{{ $developer->name}}</td>
                    <td>{{ $developer->icnumber}}</td>
                    <td>{{ $developer->streetname}}</td>
                    <td>{{ $developer->postcode}}</td>
                    <td>{{ $developer->state}}</td>
                    <td>{{ $developer->email}}</td>
                    <td>{{ $developer->phonenumber}}</td>
                    <td>{{ $developer->startworking}}</td>
                    <td>{{ $developer->skills}}</td>
                    <td>{{ $developer->status}}</td>
                    <td><a href="/developer/{{$developer->id}}" class="btn btn-info">Show</a></td>
                </tr>
            @endforeach
            <table>
    </div> --}}


@extends('layouts.app')

@section('content')
                
<div class="container">
                <div class="form-group">
                    <label class="col-md-4 control-label" for="button1id"></label>
                        <div class="col-md-8">
                            <div class="btn-group">
                                <a href="{{ url('/') }}" class="btn btn-danger"><i class="fas fa-angle-left"></i> Back to homepage </a> 
                </div>
                <div class="btn-group">
                        <a href="/developer/create" class="btn btn-info"><i class="fas fa-user-plus"></i></i> Add new developer </a>
                </div><br><br>

</div>


                <div class="container">
                    <div class="row justify-content-center">
                    <div class="col-md-12 shadow">
                    <div class="card text-white bg-dark mb-3">
                    <div class="card-header shadow"><h5> <i class="fas fa-list-alt"></i> List of developer</h5></div>
                    <div class="card-body shadow">
                    <table class="table table-hover">
                    <thead>
                            <tr>
                                    <td>Name</td>
                                    {{-- <td>IC Number</td>
                                    <td>Street Name</td>
                                    <td>Postcode</td>
                                    <td>State</td>
                                    <td>Email</td>
                                    <td>Phone Number</td>
                                    <td>Start Working Date</td>
                                    <td>Skills</td> --}}
                                    <td>Status</td>
                                    <td>Action</td>
                                  </tr>
                    </thead>
                    <tbody>
                            @foreach($developer as $developer)
                            <tr>
                                <td>{{ $developer->name}}</td>
                                {{-- <td>{{ $developer->icnumber}}</td>
                                <td>{{ $developer->streetname}}</td>
                                <td>{{ $developer->postcode}}</td>
                                <td>{{ $developer->state}}</td>
                                <td>{{ $developer->email}}</td>
                                <td>{{ $developer->phonenumber}}</td>
                                <td>{{ $developer->startworking}}</td>
                                <td>{{ $developer->skills}}</td> --}}
                                <td>{{ $developer->status}}</td>
                                <td><div class="btn-group"><a href="/developer/{{$developer->id}}" class="btn btn-info"><i class="fas fa-info"></i></a></div>
                                <div class="btn-group"><form action="{{action('DeveloperController@destroy', $developer['id'])}}"
                                    method="post">
    
                                            @csrf
                                            <input name="_method" type="hidden" value="DELETE">
                                            <button class="btn btn-danger" type="submit"><i class="fas fa-user-minus"></i></button>
                                            </form></div>
                                            </td>
                                            
                            </tr>
                        @endforeach
                    </tbody>
                    </table>
                    </div>
                    </div>
                    </div>
                    </div>
                </div>
            
                    
@endsection
