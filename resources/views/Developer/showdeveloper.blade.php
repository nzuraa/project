{{-- <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Developers List</title>
<link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>

<body>
    <div class="container">
<br />
@if (\Session::has('success'))
<div class="alert alert-success">
<p>{{ \Session::get('success') }}</p>
</div><br />
@endif


</div>
</body>
</html>
    <div class="container">


        <label>Name:</label>{{$developer->name}}<br>
        <label>IC Number:</label>{{$developer->icnumber}}<br>
        <label>Address:</label>{{$developer->address}}<br>
        <label>Phone Number:</label>{{$developer->phonenumber}}<br>


        <form method="POST" action="/developer/{{$developer->id}}">
            @csrf
            @method('DELETE')
            <a href="/developer/{{$developer->id}}/edit" class="btn btn-info">Edit</a>
            <button class="btn btn-danger" type="submit" onclick="return confirm('Are you sure you want to DELETE this item?');">DELETE</button>
        </form>
    </div> --}}




@extends('layouts.app')


@section('content') 

<div class="container">
    <a href="{{ url('/developer') }}" class="btn btn-danger"><i class="fas fa-angle-left"></i> Back to Developer</a> 
</div><br>

<div class="container">
    <div class="card  text-white bg-dark mb-3 shadow">
    <h5 class="card-header"><i class="fas fa-user"></i> Your Developer</h5>
    <div class="card-body">
        <div class="container">

            <br>

            <h2 class="text-center"> YOUR DEVELOPER'S DETAIL </h2>

            <br><br>


            <form>
                <div class="form-group row">
                    <label for="name" class="col-4 col-form-label">Name</label> 
                    <div class="col-8">
                      <div class="input-group">
                        <div class="input-group-addon">
                        </div> 
                        {{$developer->name}}
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                      <label for="icnumber" class="col-4 col-form-label">Identification No.</label> 
                      <div class="col-8">
                        {{$developer->icnumber}}
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="streetname" class="col-4 col-form-label">Street Name</label> 
                        <div class="col-8">
                          {{$developer->streetname}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="postcode" class="col-4 col-form-label">Postcode</label> 
                        <div class="col-8">
                          {{$developer->postcode}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="state" class="col-4 col-form-label">State</label> 
                        <div class="col-8">
                            {{$developer->state}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-4 col-form-label">Email Address</label> 
                        <div class="col-8">
                            {{$developer->email}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="phonenumber" class="col-4 col-form-label">Phone No.</label> 
                        <div class="col-8">
                          {{$developer->phonenumber}}
                        </div>
                   </div>
                   <div class="form-group row">
                      <label for="workingdate" class="col-4 col-form-label">Start working date</label> 
                      <div class="col-8">
                          {{$developer->startworking}}
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="skills" class="col-4 col-form-label">Skill</label> 
                        <div class="col-8">
                          {{$developer->skills}}
                        </div>
                      </div> 
                      <div class="form-group row">
                          <label class="col-4">Status</label> 
                          <div class="col-8">
                            {{$developer->status}}
                          </div>
                        </div> 
                    
                    <div class="form-group row">
                      <div class="offset-4 col-8">
                            <div class="btn-group"><a href="{{action('DeveloperController@edit', $developer['id'])}}" class="btn btn-warning"><i class="fas fa-user-edit"></i> Edit Detail</a></div>
                      </div>
                    </div>
                  </form>

@endsection