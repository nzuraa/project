@extends('layouts.app')

@section('content')
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">Dashboard</div>

								<div class="card-body">
@if(session('status'))
									<div class="alert alert-success" role="alert">
										{{ session('status') }}
									</div>
@endif

									<br/><br/>
									<div class="row justify-content-center">
										<div class="col-md-4 text-md-center">
											<a href="{{ url('/administrator/lecturers') }}" type="button" class="btn btn-primary btn-lg">Manage<br/>lecturers</a>
										</div>
										<div class="col-md-4 text-md-center">
											<a href="{{ url('/administrator/students') }}" type="button" class="btn btn-primary btn-lg">Manage<br/>students</a>
										</div>
										<div class="col-md-4 text-md-center">
											<a href="{{ url('/administrator/companies') }}" type="button" class="btn btn-primary btn-lg">Manage<br/>companies</a>
										</div>
									</div>
									<br/><br/><br/>
									<div class="row justify-content-center">
										<div class="col-md-6 text-md-center">
											<a href="{{ url('/administrator/employments') }}" type="button" class="btn btn-danger btn-lg">Manage<br/>student<br/>employments</a>
										</div>
										<div class="col-md-6 text-md-center">
											<a href="{{ url('/administrator/semesters') }}" type="button" class="btn btn-danger btn-lg">Manage<br/>semesters<br/>and dates</a>
										</div>
									</div>
									<br/><br/>
								</div>
							</div>
						</div>
					</div>
				</div>
@endsection
