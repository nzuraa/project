
<!DOCTYPE html>
<html>
<head>
    @include('layouts.app')


    @section('content') 
<meta charset="utf-8">
<title>Data</title>
<link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>




<!--
<body>
     <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger" role="alert">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
          -->
<form class="form-horizontal">

  <fieldset>

<!-- Form Name -->
<body>
    <div class="container">
     <div class="card">
      <h5 class="card-header"><legend>Developer Form</legend></h5>
      <div class="card-body">
     <div class="container">


<!-- Text input-->
<form method="post" action="{{action('DeveloperController@store')}}" enctype="multipart/form-data">
  @csrf

{{-- <div class="form-group">
  <label class="col-md-4 control-label" for="Name">Name</label>
  <div class="col-md-4">
  <input id="textinput" name="name" type="text"  class="form-control input-md">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="icnumber">IC Number</label>
  <div class="col-md-4">
  <input id="textinput" name="icnumber" type="text"  class="form-control input-md">

  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="address">Address</label>
  <div class="col-md-4">
    <textarea class="form-control" id="" name="address"></textarea>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Email">Email Address</label>
  <div class="col-md-4">
  <input id="textinput" name="email" type="text"  class="form-control input-md">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Phone">Phone Number</label>
  <div class="col-md-4">
  <input id="textinput" name="phonenumber" type="text"  class="form-control input-md">

  </div>
</div>

<!-- Search input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Start Working Date">Start Working Date</label>
  <div class="col-md-4">
    <input id="text" name="startworking" type="text"  class="form-control input-md">

  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="textarea">Skills</label>
  <div class="col-md-4">
    <textarea class="form-control" id="textarea" name="skills"></textarea>
  </div>
</div>

<!-- Multiple Radios -->
<div class="form-group">
  <label class="col-md-4 control-label" for="status">Job Status</label>
  <div class="col-md-4">
  <div class="radio">
    <label for="radios-0">
      <input type="radio" name="status" id="status" value="1" >
      Fulltime
    </label>
	</div>
  <div class="radio">
    <label for="radios-1">
      <input type="radio" name="status" id="status" value="2">
      Part Time
    </label>
	</div>
  </div>
</div> --}}

<form>
  <div class="form-group row">
    <label for="name" class="col-4 col-form-label">Name</label> 
    <div class="col-8">
      <div class="input-group">
        <div class="input-group-addon">
          <i class="fa fa-address-card"></i>
        </div> 
        <input id="name" name="name" placeholder="eg: Ali" type="text" class="form-control here" required="required">
      </div>
    </div>
  </div>
  <div class="form-group row">
    <label for="icnumber" class="col-4 col-form-label">Identification No.</label> 
    <div class="col-8">
      <input id="icnumber" name="icnumber" placeholder="eg: 960101-56-4949" type="text" class="form-control here" required="required">
    </div>
  </div>
  <div class="form-group row">
    <label for="streetname" class="col-4 col-form-label">Street Name</label> 
    <div class="col-8">
      <input id="streetname" name="streetname" placeholder="eg: Taman Seri Serdang" type="text" class="form-control here" required="required">
    </div>
  </div>
  <div class="form-group row">
    <label for="postcode" class="col-4 col-form-label">Postcode</label> 
    <div class="col-8">
      <input id="postcode" name="postcode" placeholder="eg: 43300" type="text" class="form-control here" required="required">
    </div>
  </div>
  <div class="form-group row">
      <label for="state" class="col-4 col-form-label">State</label> 
      <div class="col-8">
          <select class="form-control here" name=state>
            <option value="johor">Johor</option>
            <option value="kedah">Kedah</option>
            <option value="kelantan">Kelantan</option>
            <option value="K.L">Kuala Lumpur</option>
            <option value="labuan">Labuan</option>
            <option value="melaka">Melaka</option>
            <option value="negeri sembilan">Negeri Sembilan</option>
            <option value="pahang">Pahang</option>
            <option value="penang">Pulau Pinang</option>
            <option value="putrajaya">Putrajaya</option>
            <option value="perak">Perak</option>
            <option value="perlis">Perlis</option>
            <option value="sabah">Sabah</option>
            <option value="sarawak">Sarawak</option>
            <option value="selangor">Selangor</option>
            <option value="terengganu">Terengganu</option>
        </select>
      </div>
    </div>
  <div class="form-group row">
    <label for="email" class="col-4 col-form-label">Email Address</label> 
    <div class="col-8">
      <input id="email" name="email" placeholder="Ali@gmail.com" type="text" class="form-control here" required="required">
    </div>
  </div>
  <div class="form-group row">
    <label for="phonenumber" class="col-4 col-form-label">Phone No.</label> 
    <div class="col-8">
      <input id="phonenumber" name="phonenumber" placeholder="0122321785" type="text" class="form-control here" required="required">
    </div>
  </div>
  <div class="form-group row">
    <label for="workingdate" class="col-4 col-form-label">Start working date</label> 
    <div class="col-8">
      <input id="workingdate" name="workingdate" type="date" class="form-control here" required="required">
    </div>
  </div>   
</div>
  <div class="form-group row">
    <label class="col-4 col-form-label">Skill</label> 
    <div class="col-8">
      <div class="form-check">
        <label class="form-check-label">
          <input name="skills" type="checkbox"  class="form-check-input" value="Python">
                Python
        </label>
      </div>
      <div class="form-check">
        <label class="form-check-label">
          <input name="skills" type="checkbox"  class="form-check-input" value="C">
                C
        </label>
      </div>
      <div class="form-check">
        <label class="form-check-label">
          <input name="skills" type="checkbox"  class="form-check-input" value="C++">
                C++
        </label>
      </div>
      <div class="form-check">
        <label class="form-check-label">
          <input name="skills" type="checkbox"  class="form-check-input" value="Java">
                Java
        </label>
      </div>
      <div class="form-check">
        <label class="form-check-label">
          <input name="skills" type="checkbox"  class="form-check-input" value="Javascript">
                Javascript
        </label>
      </div>
      <div class="form-check">
        <label class="form-check-label">
          <input name="skills" type="checkbox"  class="form-check-input" value="C">
                C
        </label>
      </div>
      <div class="form-check">
        <label class="form-check-label">
          <input name="skills" type="checkbox"  class="form-check-input" value="C++">
                C++
        </label>
      </div>
      <div class="form-check">
        <label class="form-check-label">
          <input name="skills" type="checkbox"  class="form-check-input" value="PHP">
                PHP
        </label>
      </div>
      <div class="form-check">
        <label class="form-check-label">
          <input name="skills" type="checkbox"  class="form-check-input" value="HTML">
                HTML
        </label>
      </div>
      <div class="form-check">
        <label class="form-check-label">
          <input name="skills" type="checkbox"  class="form-check-input" value="CSS">
                CSS
        </label>
      </div>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-4">Status</label> 
    <div class="col-8">
      <div class="form-check form-check-inline">
        <label class="form-check-label">
          <input name="status" type="radio" required="required" class="form-check-input" value="Full Time">
                Full Time
        </label>
      </div>
      <div class="form-check form-check-inline">
        <label class="form-check-label">
          <input name="status" type="radio" required="required" class="form-check-input" value="Part Time">
                Part Time
        </label>
      </div>
    </div>
  </div> 


  <div class="form-group row">
      <div class="offset-4 col-8">
        <button name="submit" type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
 
<!-- Button (Double) -->
{{-- <div class="form-group">
  <label class="col-md-4 control-label" for="button1id"></label>
  <div class="col-md-8">
    <div class="btn-group">
        <button type="submit" class="btn btn-primary">Submit</button>

    </div>
    <div class="btn-group">
    <button type="submit" class="btn btn-danger">Cancel</button>

  </div>
</div> --}}

</fieldset>
</form>



-----------------------------------------------------------------------------------------------






@extends('layouts.app')


@section('content') 

<div class="container">
    <a href="{{ url('/developer') }}" class="btn btn-danger">< Back</a> 
</div><br>

<div class="container">
    <div class="card">
    <h5 class="card-header">Create New Developer</h5>
    <div class="card-body">
        <div class="container">
            <form method="post" action="{{action('DeveloperController@store')}}" enctype="multipart/form-data">
            @csrf

            
            

            <br>

            <h2 class="text-center"> Your New Developer </h2>


            <br><br>


            <form>
                <div class="form-group row">
                  <label for="name" class="col-4 col-form-label">Name</label> 
                  <div class="col-8">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-address-card"></i>
                      </div> 
                      <input id="name" name="name" placeholder="eg: Ali" type="text" class="form-control here" required="required">
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="icnumber" class="col-4 col-form-label">Identification No.</label> 
                  <div class="col-8">
                    <input id="icnumber" name="icnumber" placeholder="eg: 960101-56-4949" type="text" class="form-control here" required="required">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="streetname" class="col-4 col-form-label">Street Name</label> 
                  <div class="col-8">
                    <input id="streetname" name="streetname" placeholder="eg: Taman Seri Serdang" type="text" class="form-control here" required="required">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="postcode" class="col-4 col-form-label">Postcode</label> 
                  <div class="col-8">
                    <input id="postcode" name="postcode" placeholder="eg: 43300" type="text" class="form-control here" required="required">
                  </div>
                </div>
                <div class="form-group row">
                    <label for="state" class="col-4 col-form-label">State</label> 
                    <div class="col-8">
                        <select class="form-control here" name=state>
                          <option value="johor">Johor</option>
                          <option value="kedah">Kedah</option>
                          <option value="kelantan">Kelantan</option>
                          <option value="K.L">Kuala Lumpur</option>
                          <option value="labuan">Labuan</option>
                          <option value="melaka">Melaka</option>
                          <option value="negeri sembilan">Negeri Sembilan</option>
                          <option value="pahang">Pahang</option>
                          <option value="penang">Pulau Pinang</option>
                          <option value="putrajaya">Putrajaya</option>
                          <option value="perak">Perak</option>
                          <option value="perlis">Perlis</option>
                          <option value="sabah">Sabah</option>
                          <option value="sarawak">Sarawak</option>
                          <option value="selangor">Selangor</option>
                          <option value="terengganu">Terengganu</option>
                      </select>
                    </div>
                  </div>
                <div class="form-group row">
                  <label for="email" class="col-4 col-form-label">Email Address</label> 
                  <div class="col-8">
                    <input id="email" name="email" placeholder="Ali@gmail.com" type="text" class="form-control here" required="required">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="phonenumber" class="col-4 col-form-label">Phone No.</label> 
                  <div class="col-8">
                    <input id="phonenumber" name="phonenumber" placeholder="0122321785" type="text" class="form-control here" required="required">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="workingdate" class="col-4 col-form-label">Start working date</label> 
                  <div class="col-8">
                    <input id="workingdate" name="workingdate" type="date" class="form-control here" required="required">
                  </div>
                </div>   
              </div>
                <div class="form-group row">
                  <label class="col-4 col-form-label">Skill</label> 
                  <div class="col-8">
                    <div class="form-check">
                      <label class="form-check-label">
                        <input name="skills" type="checkbox"  class="form-check-input" value="Python">
                              Python
                      </label>
                    </div>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input name="skills" type="checkbox"  class="form-check-input" value="C">
                              C
                      </label>
                    </div>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input name="skills" type="checkbox"  class="form-check-input" value="C++">
                              C++
                      </label>
                    </div>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input name="skills" type="checkbox"  class="form-check-input" value="Java">
                              Java
                      </label>
                    </div>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input name="skills" type="checkbox"  class="form-check-input" value="Javascript">
                              Javascript
                      </label>
                    </div>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input name="skills" type="checkbox"  class="form-check-input" value="C">
                              C
                      </label>
                    </div>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input name="skills" type="checkbox"  class="form-check-input" value="C++">
                              C++
                      </label>
                    </div>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input name="skills" type="checkbox"  class="form-check-input" value="PHP">
                              PHP
                      </label>
                    </div>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input name="skills" type="checkbox"  class="form-check-input" value="HTML">
                              HTML
                      </label>
                    </div>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input name="skills" type="checkbox"  class="form-check-input" value="CSS">
                              CSS
                      </label>
                    </div>
                  </div>
                
                <div class="form-group row">
                  <label class="col-4">Status</label> 
                  <div class="col-8">
                    <div class="form-check form-check-inline">
                      <label class="form-check-label">
                        <input name="status" type="radio" required="required" class="form-check-input" value="Full Time">
                              Full Time
                      </label>
                    </div>
                    <div class="form-check form-check-inline">
                      <label class="form-check-label">
                        <input name="status" type="radio" required="required" class="form-check-input" value="Part Time">
                              Part Time
                      </label>
                    </div>
                  </div>
                </div> 
              
              
                <div class="form-group row">
                    <div class="offset-4 col-8">
                      <button name="submit" type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </div>

                </form>


@endsection
