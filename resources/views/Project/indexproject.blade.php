@extends('layouts.app')

@section('content')
                    
    <div class="container">
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="button1id"></label>
                            <div class="col-md-8">
                                <div class="btn-group">
                                    <a href="{{ url('/') }}" class="btn btn-danger"><i class="fas fa-angle-left"></i> Back to homepage </a> 
                    </div>
                    <div class="btn-group">
                            <a href="/project/create" class="btn btn-info"><i class="fas fa-user-plus"></i></i> Add new project </a>
                    </div><br><br>
    
    </div>
    
    
                    <div class="container">
                        <div class="row justify-content-center">
                        <div class="col-md-12 shadow">
                        <div class="card text-white bg-dark mb-3">
                        <div class="card-header shadow"><h5> <i class="fas fa-list-alt"></i> List of project</h5></div>
                        <div class="card-body shadow">
                        <table class="table table-hover">
                        <thead>
                                <tr>
                                        <td>Project Name</td>
                                        <td>Project Customer</td>
                                        <td>Lead Developer</td>
                                        <td>Status</td>
                                        <td>Action</td>
                                      </tr>
                        </thead>
                        <tbody>
                                @foreach($project as $project)
                                <tr>
                                    <td>{{ $project->projectname}}</td>
                                    <td>{{ $project->customer}}</td>
                                    <td>{{ $project->developer->name}}</td>
                                    <td>{{ $project->status}}</td>
                                    <td><div class="btn-group"><a href="/project/{{$project->id}}" class="btn btn-info"><i class="fas fa-info"></i></a></div>
                                </tr>
                            @endforeach
                        </tbody>
                        </table>
                        </div>
                        </div>
                        </div>
                        </div>
                    </div>
                
                        
    @endsection
    