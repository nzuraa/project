@extends('layouts.app')


@section('content') 

<div class="container">
    <a href="{{ url('/project') }}" class="btn btn-danger"><i class="fas fa-angle-left"></i> Back to Project </a> 
</div><br>

<div class="container">
    <div class="card  text-white bg-dark mb-3">
    <h5 class="card-header shadow"><i class="fas fa-briefcase"></i> Project : {{ $project->projectname }}  </h5>
    <div class="card-body shadow">
        <div class="container">

            
            

            <br>

            <h2 class="text-center"> YOUR PROJECT'S DETAIL </h2>

            <br><br>


                <div class="form-group row">
                    <label for="projectname" class="col-4 col-form-label">Project Name</label> 
                    <div class="col-8">
                      <div class="input-group">
                        <div class="input-group-addon">
                        </div>
                             {{ $project->projectname }}
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                      <label for="projectdescription" class="col-4 control-label col-4">Project Description</label> 
                      <div class="col-8">
                        {{ $project->projectdescription }}
                      </div>
                    </div> 
                  <div class="form-group row">
                      <label for="customer" class="col-4 col-form-label">Customer</label> 
                      <div class="col-8">
                        {{$project->customer}}
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="startdate" class="col-4 col-form-label">Start date</label> 
                      <div class="col-8">
                        {{$project->startdate}}
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="enddate" class="col-4 col-form-label">End date</label> 
                        <div class="col-8">
                        {{$project->enddate}}
                        </div>
                      </div>
                      <div class="form-group row">
                          <label for="developer_id" class="col-4 col-form-label">Lead Developer</label>
                          <div class="col-8">
                          {{$project->developer->name}}
                          </div>
                        </div>  

                      
                        
                    <div class="container">
                        <div class="card  text-white bg-dark mb-3">
                        <div class="card-body shadow">
                            <div class="container">
                    
                                
                                
                    
                                <br>
                    
                                <h2 class="text-center"><i class="fas fa-thumbtack"></i> {{$project->projectname}}'s Task </h2>
                    
                                <br><br>

                                <div class="container">
                                    <div class="row justify-content-center">
                                    <div class="col-md-12">
                                    <div class="card text-white bg-dark mb-3">
                                    <div class="card-body">
                                    <table class="table table-hover">
                                    <thead>
                                            <tr>
                                                    <td>Task Name</td>
                                                    <td>Delivered by</td>
                                                    <td>Task status</td>
                                                  </tr>
                                    </thead>
                                    <tbody>
                                            @foreach ($project->tasks as $task)
                                            <tr>
                                                <td>{{$task->taskname}}</td>
                                                <td>{{$task->developer->name}}</td>
                                                <td>{{$task->status}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    </table>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                        <div class="offset-4 col-8">
                                              <div class="btn-group"><a href="{{action('ProjectController@edit', $project['id'])}}" class="btn btn-warning"><i class="fas fa-user-edit"></i> Edit Detail</a></div>
                                        </div>
                                      </div>

@endsection