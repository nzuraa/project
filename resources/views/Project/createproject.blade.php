@extends('layouts.app')


@section('content') 

<div class="container">
    <a href="{{ url('/project') }}" class="btn btn-danger"><i class="fas fa-angle-left"></i> Back to Project </a> 
</div><br>

<div class="container">
    <div class="card  text-white bg-dark mb-3">
    <h5 class="card-header shadow"><i class="fas fa-user-plus"></i> Creating New Project</h5>
    <div class="card-body shadow">
        <div class="container">
            <form method="post" action="{{action('ProjectController@store')}}" enctype="multipart/form-data">
            @csrf

            
            

            <br>

            <h2 class="text-center"> YOUR NEW PROJECT </h2>

            <br><br>


            <form>
                <div class="form-group row">
                    <label for="projectname" class="col-4 col-form-label">Project Name</label> 
                    <div class="col-8">
                      <div class="input-group">
                        <div class="input-group-addon">
                        </div> 
                        <input id="projectname" name="projectname" type="text" class="form-control here" required="required">
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                      <label for="projectdescription" class="col-4 control-label col-4">Project Description</label> 
                      <div class="col-8">
                        <textarea id="projectdescription" name="projectdescription" cols="40" rows="5" class="form-control here" required="required"></textarea>
                      </div>
                    </div> 
                  <div class="form-group row">
                      <label for="customer" class="col-4 col-form-label">Customer</label> 
                      <div class="col-8">
                        <input id="customer" name="customer" type="text" class="form-control here" required="required">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="startdate" class="col-4 col-form-label">Start date</label> 
                      <div class="col-8">
                        <input id="startdate" name="startdate" type="date" class="form-control here" required="required">
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="enddate" class="col-4 col-form-label">End date</label> 
                        <div class="col-8">
                          <input id="enddate" name="enddate" type="date" class="form-control here" required="required">
                        </div>
                      </div>
                      <div class="form-group row">
                          <label for="developer_id" class="col-4 col-form-label">Lead Developer</label>
                          <div class="col-8">
                          <select class="form-control" id="developer_id" name="developer_id">
                              @foreach ($developer as $developer)
                                  <option value={{$developer->id}}>{{$developer->name}}</option>  
                              @endforeach
                          </select>
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="status" class="col-4 col-form-label">Project Status</label> 
                          <div class="col-8">
                            <div class="checkbox">
                              <label class="checkbox">
                                <input type="radio" name="status" value="Risk">
                                    Risk
                              </label>
                            </div>
                            <div class="checkbox">
                              <label class="checkbox">
                                <input type="radio" name="status" value="Potential risk">
                                    Potential risk
                              </label>
                            </div>
                            <div class="checkbox">
                              <label class="checkbox">
                                <input type="radio" name="status" value="On track">
                                    On track
                              </label>
                            </div>
                          </div>
                        </div> <br>
                                     
                    <div class="form-group row">
                      <div class="offset-4 col-8">
                        <button name="submit" type="submit" class="btn btn-primary">Submit</button>
                      </div>
                    </div>
                  </form>

@endsection