@extends('layouts.app')

@section('content')
<div class="container">
    <div class="jumbotron jumbotron-fluid bg-dark text-white shadow">
        <div class="container text-center">
            <h1 class="display-4"><i class="fab fa-connectdevelop"></i> LOGIKTAK</h1>
            <p class="lead" >By Aiman Amir & Haedah</p>
        </div>
    </div>
</div>
@endsection
