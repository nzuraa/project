@extends('layouts.app')


@section('content') 

<div class="container">
    <a href="{{ url('/task') }}" class="btn btn-danger"><i class="fas fa-angle-left"></i> Back to Task </a> 
</div><br>

<div class="container">
    <div class="card  text-white bg-dark mb-3">
    <h5 class="card-header shadow"><i class="fas fa-thumbtack"></i> Your Task</h5>
    <div class="card-body shadow">
        <div class="container">
            <form method="post" action="{{action('TaskController@update', $tasks->id)}}" enctype="multipart/form-data">
                <input name="_method" type="hidden" value="PATCH">
            @csrf

            
            
    <form>
            <br>

            <h2 class="text-center"> YOUR TASK'S DETAIL </h2>

            <br><br>


            
                <div class="form-group row">
                    <label for="projectname" class="col-4 col-form-label">Task Name</label> 
                    <div class="col-8">
                      <div class="input-group">
                        <div class="input-group-addon">
                        </div> 
                        {{$tasks->taskname}}
                      </div>
                    </div>
                </div>
                    <div class="form-group row">
                            <label for="tasktdescription" class="col-4 control-label col-4">Task Description</label> 
                            <div class="col-8">
                              {{$tasks->taskdescription}}
                            </div>
                </div>
                  <div class="form-group row">
                        <label for="startdate" class="col-4 col-form-label">Start date</label> 
                        <div class="col-8">
                            {{$tasks->startdate}}
                        </div>
                      </div>
                      <div class="form-group row">
                          <label for="enddate" class="col-4 col-form-label">End date</label> 
                          <div class="col-8">
                            {{$tasks->enddate}}
                          </div>
                        </div>
                  <div class="form-group row">
                    <label for="projectname" class="col-4 col-form-label">Project</label>
                    <div class="col-8">
                            {{$tasks->project->projectname}}
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-4 col-form-label">Developer</label>
                    <div class="col-8">
                            {{$tasks->developer->name}}
                    </div>
                </div>
                <div class="form-group row">
                        <label for="status" class="col-4 col-form-label">Task Status</label> 
                        <div class="col-8">
                            {{$tasks->status}}
                        </div>
                      </div> <br>
                     
                   
                        

                      <div class="form-group row">
                            <div class="offset-4 col-8">
                                  <div class="btn-group"><a href="{{action('TaskController@edit', $tasks['id'])}}" class="btn btn-warning"><i class="fas fa-user-edit"></i> Edit Detail</a></div>
                            </div>
                          </div>

                  </form>

@endsection