@extends('layouts.app')

@section('content')
                    
    <div class="container">
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="button1id"></label>
                            <div class="col-md-8">
                                <div class="btn-group">
                                    <a href="{{ url('/') }}" class="btn btn-danger"><i class="fas fa-angle-left"></i> Back to homepage </a> 
                    </div>
                    <div class="btn-group">
                            <a href="/task/create" class="btn btn-info"><i class="fas fa-user-plus"></i></i> Add new task </a>
                    </div><br><br>
    
    </div>
    
    
                    <div class="container">
                        <div class="row justify-content-center">
                        <div class="col-md-12 shadow">
                        <div class="card text-white bg-dark mb-3">
                        <div class="card-header shadow"><h5> <i class="fas fa-thumbtack"></i> List of Task</h5></div>
                        <div class="card-body shadow">
                        <table class="table table-hover">
                        <thead>
                                <tr>
                                        <td>Task Name</td>
                                        <td>Project</td>
                                        <td>Developer</td>
                                        <td>Status</td>
                                        <td>Action</td>
                                      </tr>
                        </thead>
                        <tbody>
                                @foreach($tasks as $task)
                                <tr>
                                    <td>{{ $task->taskname}}</td>
                                    <td>{{ $task->project->projectname}}</td>
                                    <td>{{ $task->developer->name}}</td>
                                    <td>{{ $task->status}}</td>
                                    <td><div class="btn-group"><a href="/task/{{$task->id}}" class="btn btn-info"><i class="fas fa-info"></i></a></div>
                                        <div class="btn-group"><form action="{{action('TaskController@destroy', $task['id'])}}"
                                            method="post">
            
                                                    @csrf
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <button class="btn btn-danger" type="submit"><i class="fas fa-trash-alt"></i></button>
                                </tr>
                            @endforeach
                        </tbody>
                        </table>
                        </div>
                        </div>
                        </div>
                        </div>
                    </div>
                
                        
    @endsection
    