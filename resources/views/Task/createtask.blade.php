@extends('layouts.app')


@section('content') 

<div class="container">
    <a href="{{ url('/task') }}" class="btn btn-danger"><i class="fas fa-angle-left"></i> Back to Task </a> 
</div><br>

<div class="container">
    <div class="card  text-white bg-dark mb-3">
    <h5 class="card-header shadow"><i class="fas fa-thumbtack"></i> Creating New Task</h5>
    <div class="card-body shadow">
        <div class="container">
            <form method="post" action="{{action('TaskController@store')}}" enctype="multipart/form-data">
            @csrf

            
            

            <br>

            <h2 class="text-center"> YOUR NEW TASK </h2>

            <br><br>


            <form>
                <div class="form-group row">
                    <label for="projectname" class="col-4 col-form-label">Task Name</label> 
                    <div class="col-8">
                      <div class="input-group">
                        <div class="input-group-addon">
                        </div> 
                        <input id="taskname" name="taskname" type="text" class="form-control here" required="required">
                      </div>
                    </div>
                </div>
                    <div class="form-group row">
                            <label for="tasktdescription" class="col-4 control-label col-4">Task Description</label> 
                            <div class="col-8">
                              <textarea id="taskdescription" name="taskdescription" cols="40" rows="5" class="form-control here" required="required"></textarea>
                            </div>
                </div>
                  <div class="form-group row">
                        <label for="startdate" class="col-4 col-form-label">Start date</label> 
                        <div class="col-8">
                          <input id="startdate" name="startdate" type="date" class="form-control here" required="required">
                        </div>
                      </div>
                      <div class="form-group row">
                          <label for="enddate" class="col-4 col-form-label">End date</label> 
                          <div class="col-8">
                            <input id="enddate" name="enddate" type="date" class="form-control here" required="required">
                          </div>
                        </div>
                  <div class="form-group row">
                    <label for="projectname" class="col-4 col-form-label">Project</label>
                    <div class="col-8">
                        <select class="form-control" id="project_id" name="project_id">
                            @foreach ($project as $project)
                                <option value={{$project->id}}>{{$project->projectname}}</option>  
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-4 col-form-label">Developer</label>
                    <div class="col-8">
                        <select class="form-control" id="developer_id" name="developer_id">
                            @foreach ($developer as $developer)
                            <option value={{$developer->id}}>{{$developer->name}}</option>  
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                        <label for="status" class="col-4 col-form-label">Task Status</label> 
                        <div class="col-8">
                          <div class="checkbox">
                            <label class="checkbox">
                              <input type="radio" name="status" value="Started" required="required">
                                  Started
                            </label>
                          </div>
                          <div class="checkbox">
                            <label class="checkbox">
                              <input type="radio" name="status" value="In progress">
                                  In progress
                            </label>
                          </div>
                          <div class="checkbox">
                            <label class="checkbox">
                              <input type="radio" name="status" value="Completed">
                                  Completed
                            </label>
                          </div>
                          <div class="checkbox">
                            <label class="checkbox">
                              <input type="radio" name="status" value="Delayed">
                                  Delayed
                            </label>
                          </div>
                        </div>
                      </div> <br>
                     
                   
                        

                    <div class="form-group row">
                      <div class="offset-4 col-8">
                        <button name="submit" type="submit" class="btn btn-primary">Submit</button>
                      </div>
                    </div>
                  </form>

@endsection