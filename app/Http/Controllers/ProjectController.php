<?php

namespace App\Http\Controllers;

use App\Project;
use App\Developer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $project = Project::all();
        return view ('Project.indexproject', compact('project'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $developer=\App\Developer::where('status', 'Full Time')->get();
        return view ('Project.createproject', compact('developer'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $project= new \App\Project;
            $project->id=$request->input('id');
            $project->projectname = $request->get('projectname');
            $project->projectdescription = $request->get('projectdescription');
            $project->customer = $request->get('customer');
            $project->status = $request->get('status');
            $project->startdate = $request->get('startdate');
            $project->enddate = $request->get('enddate');
            $project->developer_id=$request->input('developer_id');
            $project->save();
            return redirect('/project');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $developer=\App\Project::find($id)->developer;
        $project=\App\Project::find($id);
        return view('Project.showproject',compact('project', 'developer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $developer =\App\Developer::all();
        $project=\App\Project::find($id);
        return view ('Project.editproject', compact ('project' , 'developer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $project= \App\Task::find($id);
        $project->projectname = $request->get('projectname');
        $project->projectdescription = $request->get('projectdescription');
        $project->customer = $request->get('customer');
        $project->status = $request->get('status');
        $project->startdate = $request->get('startdate');
        $project->enddate = $request->get('enddate');
        $project->developer_id=$request->input('developer_id');
        $project->save();
        return redirect('/project');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
