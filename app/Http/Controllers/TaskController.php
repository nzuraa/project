<?php

namespace App\Http\Controllers;

use App\Task;
use App\Project;
use App\Developer;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::all();
        return view ('Task.indextask', compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $developer=\App\Developer::all();
        $project=\App\Project::all();
        return view ('Task.createtask', compact('developer' , 'project'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $tasks= new \App\Task;
            $tasks->id=$request->input('id');
            $tasks->taskname = $request->get('taskname');
            $tasks->taskdescription = $request->get('taskdescription');
            $tasks->startdate = $request->get('startdate');
            $tasks->enddate = $request->get('enddate');
            $tasks->project_id=$request->input('project_id');
            $tasks->developer_id=$request->input('developer_id');
            $tasks->status=$request->input('status');
            $tasks->save();
            return redirect('/task');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tasks =\App\Task::find($id);
        return view('Task.showtask',compact('tasks'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tasks =\App\Task::find($id);
        $developer=\App\Developer::all();
        $project=\App\Project::all();
        return view ('Task.edittask', compact ('tasks', 'developer', 'project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tasks= \App\Task::find($id);
        $tasks->taskname = $request->get('taskname');
        $tasks->taskdescription = $request->get('taskdescription');
        $tasks->startdate = $request->get('startdate');
        $tasks->enddate = $request->get('enddate');
        $tasks->project_id=$request->input('project_id');
        $tasks->developer_id=$request->input('developer_id');
        $tasks->status=$request->input('status');
        $tasks->save();
        return redirect('/task');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tasks = \App\Task::find($id);
        $tasks->delete();
        return redirect('/task')->with('Success','Your developer has been deleted');
    
    }
}
