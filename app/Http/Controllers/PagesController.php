<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome()
    {
        return view ('welcome');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function contact()
    {
      $name = "Amir_Haedah";
      $hp = "0139614637";
      return view('about',['name'=>$name, 'hp'=>$hp]);
    }

    public function product(Request $request)
    {
      $lists = [
          'one',
          'two',
          'three'
        ];
      return view('product',['lists'=>$lists]);

    }

  }
