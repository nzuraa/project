<?php

namespace App\Http\Controllers;
use App\Developer;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DeveloperController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $developer = Developer::all();
        return view ('Developer.indexdeveloper', compact('developer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('Developer.createdeveloper');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        {
            $developer= new \App\Developer;
            $developer->id=$request->input('id');
            $developer->name = $request->get('name');
            $developer->icnumber = $request->get('icnumber');
            $developer->streetname = $request->get('streetname');
            $developer->postcode = $request->get('postcode');
            $developer->state = $request->get('state');
            $developer->email = $request->get('email');
            $developer->phonenumber = $request->get('phonenumber');
            $developer->startworking = $request->get('startworking');
            $developer->skills = $request->get('skills');
            $developer->status = $request->get('status');
            $developer->save();
            return redirect('/developer');
          }
      
      // retrieve all POST data

      // validate
      request()->validate([
          'name' => 'required | min:6',
          'icnumber' => 'required | min:3',
          'address'=> 'required',
          'phonenumber' => 'required',
          'startworking' => 'required',
          'skills' => 'required',
          'status' => 'required'
      ]);

      Developer::create([
              'name' => request('name'),
              'icnumber' => request('icnumber'),
              'address'=> request ('address'),
              'phonenumber' => request ('phonenumber'),
              'startworking' => request ('startworking'),
              'skills' => request ('skills'),
              'status' => request ('status')

          ]);

      // redirect too /lecturer

        return redirect('/indexdeveloper', compact('developer'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Developer $developer)
    {
        return view('Developer.showdeveloper',compact('developer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $developer =\App\Developer::find($id);
        return view ('Developer.editdeveloper', compact ('developer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $developer= \App\Developer::find($id);
      $developer->name = $request->get('name');
      $developer->icnumber = $request->get('icnumber');
      $developer->streetname = $request->get('streetname');
      $developer->postcode = $request->get('postcode');
      $developer->state = $request->get('state');
      $developer->phonenumber = $request->get('phonenumber');
      $developer->startworking = $request->get('startworking');
      $developer->skills = $request->get('skills');
      $developer->status = $request->get('status');
      $developer->save();
      return redirect('/developer');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $developer = \App\Developer::find($id);
        $developer->delete();
        return redirect('/developer')->with('Success','Your developer has been deleted');
    }
}
