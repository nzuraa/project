<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public function developer()
    {
        return $this->belongsTo(Developer::class);
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }
}
